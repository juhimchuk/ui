﻿app.directive('myExample', function () {
    return {
        restrict: 'AE',
        replace: true,
        template: '<h1>Heavy Sand - Creative Photography</h1> '
    }
});
app.directive('myText', function () {
    return {
        restrict: 'AE',
        replace: true,
        template: "<p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshops version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</p>"
    }
});
app.directive('myTextEnd', function () {
    return {
        restrict: 'AE',
        replace: true,template:  "<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>"

    }
});
app.directive('listExample', function () {
    return {
        templateUrl: 'js/directives/list.html'
       
    }
});
app.directive('templateExample', function () {
    return {
        // @ - string
        // < - variable
        // = - function
        scope: {
            anotherVar: '<',
            onButtonClick: '='
        },
        templateUrl: 'js/directives/modal.html'
       
    }
});