﻿app.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');

        $routeProvider
            .when('/home', {
                templateUrl: 'app/templates/homepage.html'
            })
            .when('/work', {
                templateUrl: 'app/templates/work.html'
            })
            .when('/single', {
                templateUrl: 'app/templates/single.html'
            })
            .when('/contact', {
                templateUrl: 'app/templates/contact.html'
                //controller: 'detailsController'
            })
            .otherwise({
                redirectTo: '/home'
            });
}]);